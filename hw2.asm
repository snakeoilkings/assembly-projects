##############################################################
# Homework #2				 #
# name: Kathryn Blecher				 #
# sbuid: 108871623				 #
##############################################################
.text

strlen:
	#starting address of string in $a0
	add $t8, $a0, $0	# copy the address
	add $v0, $0, $0	# initialize count 
strlen_loop:
	lb $t0, ($t8)	# load first char of string
	beqz $t0, end_strlen	# if NULL char, stop counting
	addi $v0, $v0, 1	# count++
	addi $t8, $t8, 1	# move address to next byte
	j strlen_loop
end_strlen:
	jr $ra

#EASY STRING FUNCTIONS

strcmp:
	la $t0, ($a0)	   # loads address of a0 in t0
	la $t1, ($a1)	   # loads address of a1 in t1
loop_strcmp:
	lb $t2, ($t0)	   # loads first char of str1 into t2
	lb $t3, ($t1)	   # loads first char of str2 into t3
	add $t4, $t2, $t3	   # adds the two chars
	beqz $t4, true_strcmp      # if they are both null terminators, jump to true
	beqz $t2, false_strcmp     # if only one is a null, return false
	beqz $t3, false_strcmp     # if only the other is a null, return false
	addi $t0, $t0, 1	   # otherwise add 1 to string address 1
	addi $t1, $t1, 1	   # then add 1 to string address 2
	beq $t2, $t3, loop_strcmp  # continue loop if equal
false_strcmp:
	li $v0, 0		   # v0 = false
	j end_strcmp
true_strcmp:
	li $v0, 1		   # v0 = true
end_strcmp:
	jr $ra

strncpy:
	la $t0, ($a0)	  # load dst into $t0
	la $t1, ($a1)    	  # load src into $t1
	li $t2, 1	 	  # set counter
strncpy_loop:
	lb $t3, ($t1)    	  # load src char into $t3	
	sb $t3, ($t0)   	  # stores the char into dst
	addi $t0, $t0, 1 	  # (mem address of dst)++
	addi $t1, $t1, 1 	  # (mem address of source)++
	addi $t2, $t2, 1 	  # count++
	beq $a2, $t2, strncpy_end # moved n bytes, jump to add a null terminator and return
	beqz $t1, src_short       # src is done, must go fill dst with null terminators
	b strncpy_loop
src_short:
	sb $0, ($t0)              # store a null terminator in dst
	addi $t0, $t0, 1	  # (mem address of dst)++
	addi $t2, $t2, 1	  # count++
	bne $t2, $a2, src_short   # loop until we reach n (n-1 technically but we add a null terminator in the next step)
strncpy_end:
	sb $0, ($t0)	  # add a null terminator to the end
	move $v0, $a0	  # put dst address in v0
	jr $ra	

indexOf:
	la $t0, ($a0)	   # load the address of the string into t0
	li $v0, -1	               # load -1 into $v0 (default answer)
	li $t2, -1	               # set counter to -1
	beqz $a0, indexOf_exit	   # if a0 is a null terminator, exit
indexOf_loop:
	addi $t2, $t2, 1           # count++
	lb $t1, ($t0)	   # load char in string to t1
	beq $t1, $0, indexOf_exit  # if null, return -1
	addi $t0, $t0, 1           # move to next char in string
	beq $t1, $a1, found        # if found, set v0 = count
	j indexOf_loop
found:
	move $v0, $t2
indexOf_exit:
	jr $ra



#MEDIUM STRING FUNCTIONS

reverse_str:
	move $t5, $a0         # load str address into t5
	move $t9, $a1         # save a2 contents in t9
	sw $ra, ($sp)         # store return address on the stack
	jal strlen	          # find string length [0,8]
	move $t8, $a1         # save a1 address in t8
	move $t6, $v0         # saves string length in t6 
	li $t4, 0	          # set up counter 
	la $t7, storage       # load some space to reverse in to
	add $t5, $t5, $t6     # go to end of string
reverse_loop:
	addi $t5, $t5, -1     # decrement string address by 1
	lb $a0, ($t5)
	sb $a0, ($t7)         # store char in space provided
	addi $t4, $t4, 1      # count++
	beq  $t4, $t6, end_reverse
	addi $t7, $t7, 1      # increment address by 1
	j reverse_loop
end_reverse:
	move $a2, $t6         # put str length in to a2
	addi $a2, $a2, 1      # add 1 for NULL
	move $v0, $t5         # save t5 position in v0 (also a0)
	la $a0, ($t5)     
	la $a1, storage
	jal strncpy           # copy reversed string into a0 [0,1,2,3]   
	move $a1, $t9         # put that stuff back
	lw $ra, ($sp)         # return address back to ra
	jr $ra	          # praise jesus

atoi:
	sw $ra, ($sp)	# put return address on the stack
	la $t9, ($a0)	# load str into t0
	li $t1, 0		# set count = 0
	la $t2, storage         # load some space to push in to
	li $t3, 48		# set lower limit
	li $t4, 57              # upper limit
	sb $t3, ($t2)           # store char
	lb $t8, ($a0)	# load char
	slt $t5, $t4, $t8	# checks ascii code if more than 57
	slt $t6, $t8, $t3	# checks to see if ascii code is less than 48
	add $t5, $t5, $t6	# adds 2 answers
	bnez $t5 next_atoi1	# jumps address forward 1 if first char isnt an int
	j atoi_jump
atoi_loop:
	lb $t8, ($a0)	# load char
	slt $t5, $t4, $t8	# checks ascii code if more than 57
	slt $t6, $t8, $t3	# checks to see if ascii code is less than 48
	add $t5, $t5, $t6	# adds 2 answers
	bnez $t5 next_atoi
atoi_jump:	
	sb $t8, ($t2)	# store char
	addi $a0, $a0, 1
	addi $t2, $t2, 1	# move address forward by 1
	addi $t1, $t1, 1	# count++
	beq $t1, $v0 next_atoi  # has every char been checked?
	j atoi_loop
next_atoi1:
	addi $t2, $t2, 1	# move address forward by 1 (missed loop)
next_atoi:
	sb $0, ($t2)
	li $t1, 1		# count = 0
	sb $0, ($t2)
	la $a0, storage         # load original address
	jal strlen		# get length of ints
	li $t3, 10		# set t3 to 10
	li $t4, 1		# count2 = 1
	addi $a0, $a0, -1
	li $t2, 0		# set to 0
check_bit:
	addi $a0, $a0, 1	# move mem address by 1
	li $t1, 1		# count = 1
	lb $t8, ($a0)	# set t8 to the MSB
	addi $t8, $t8, -48	# turn it into its integer
	beq $v0, $t4, end_atoi  # if its at 1, were done
power: 
	mult $t3, $t8	# multiply by ten
	mflo $t8
	addi $t1, $t1, 1	# count++ (exponent)
	bne $v0, $t1, power
	addi $v0, $v0, -1	# decrement exponent
	add $t2, $t2, $t8	# add em up
	sb $t8, ($a0)	# store number
	b check_bit		# looooooooooooopppppp
end_atoi:
	add $t2, $t2, $t8	# add LSB to total
	move $v0, $t2	# put in return register
	move $a0, $t9           # put that stuff back
	lw $ra, ($sp)     	# return address back to ra
	jr $ra			

#HARD STRING FUNCTIONS

cut:
	addi $sp, $sp, -12	# set up stack for a0-a3 & ra
	sw $ra, ($sp)	# store return address on the stack
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $a2, 12($sp)
	la $t4, ($a0)	# store a0 address in t4
	la $t3, ($a1)	# store a1 address in t3
	move $a0, $a1	# put a1 into a0
	jal strlen	
	move $t6, $v0	# save str length in t6
	li $t5, 0		# this will be our adder 
	li $v1, 0		# v1 exists as a flag for which loop we're in, once we find the
	# start index, we set v1 to 1 so that when we find the end, we check v0 for 1 to go to end
check_cut:
	move $t3, $a1	# if this is 2nd+ round in loop, need to reset a1 to its original state
	add $t4, $t4, $t5	# take original string address and add adder
	lb $t7, ($a1)	# load first char from a1 
	la $a0, ($t4)	# load str from current address
	move $a1, $t7	# put first char in a0
	jal indexOf		# checks string from adder pos of first index of a1
	la $a1, ($t3)	# puts t3 into a1 for safe keeping (a1=a1 again)
	addi $v0, $v0, 1	# v0 = index of this char, +1 to it to go to next char
	beqz $v0, end_cut	# if the value cant be found, end
	add $t4, $t4, $v0	# address of char after index
	beq $t6, 1, locate_cut  # if the length of a1 is only 1, no more formal checks needed, skip ahead
	li $t7, 1		# set counter for additional chars
full_check_loop:
	lb $t9, ($t4)	# load char into t9
	addi $t4, $t4, 1	# move a0 address forward 1
	addi $a1, $a1, 1	# move second char address in a1
	lb $t8, ($a1)	# load 2nd char of a1 into t8
	add $t5, $t5, $v0	# add up to index to start searching after first occurance
	bne $t8, $t9, check_cut # if not equal, restart seach at address + adder
	addi $t7, $t7, 1	# count++
	beq $t6, $t7, locate_cut# after you check every char you can leave loop
	j full_check_loop	# looooopppp
locate_cut:  #### congrats! you have start index for your cut at t4 ####
	beq $v1, 1, finish	# if v1 is 1, we have start & finish index, jump to end
	sub $t4, $t4, $t6	# move back to start of cut
	move $t3, $a1	# puts a1 into t3
	move $a1, $a2	# puts a2 into a1
	sw $t4, point	# puts our start index into memory. why did i do this
	li $t5, 0		# reset adder to zero for new search
	move $a0, $a1	# move exit string into a0
	jal strlen		# check length of 2nd string 
	move $t6, $v0	# save str length in t6
	li $v1, 1		# mark flag for SUCCESS
	j check_cut		# start the process all over for exit-string
finish:
	la $t6, point	# load start-index to t6
	lw $v0, point	# then put it in v0
	sub $t4, $t4, $v0	# take the exit-address and minus the start address
	move $v1, $t4	# move start-address to v1
	j final
end_cut:
	li $v0, 0
	li $v1, -1
final:
	lw $a2, 12($sp)	# WERE DONE. return everything to its address
	lw $a1, 8($sp)
	lw $a0, 4($sp)
	lw $ra, ($sp)     	# return address back to ra
	addi $sp, $sp, 12
	jr $ra
	
strtok:
	sw $ra, ($sp)  	# store return address on the stack
	bnez $a0, first_run	# if not zero, its a fresh run
	lw $a0, return_address	# if not, store the info in return_address to a0
	jal indexOf		# find the delimiter
	move $a0, $0	# put a0 back the way you found it
	lw $t4, return_address	# put address in t4
	j next_strtok	# ... continue ...
first_run:
	la $t4, ($a0)	# load address of the str to t4
	jal indexOf		# find the delimiter
	sw $a0, return_address	# store this address into memory
next_strtok:
	beq $v0, -1, not_found  # if not found, jump to end
	add $t4, $t4, $v0	# go to delimiter
	sb $0, ($t4)	# put a null terminator there
	addi $t4, $t4, 1	# move address forward 1
	lw $v0, return_address	# stores the address
	sw $t4, return_address	# store this address into memory
	j found_strtok
not_found:
	lw $v0, return_address	# stores the address to v0
	beqz $v0, set_up_add	# if v0 is NULL, this is the final call (go set up address)
	sw $t4, ra_backup	# stores a backup copy of the final address
	sw $0, return_address	# sets return address to NULL
	j found_strtok	# ... continue ...
set_up_add:
	lw $a0, ra_backup	#######################################
	jal strlen		#  This section puts an address in to #
	add $a0, $a0, $v0	#    return_address after next NULL   #
	addi $a0, $a0, 1	#  			  #
	sw $a0, return_address	#    This whole section feels	  #
	move $a0, $0	#         so so very wrong	  #
	add $v0, $0, $0	#######################################
found_strtok:
	lw $ra, ($sp)     	# return address back to ra	
	jr $ra

end:

.data

storage: .space 400
point: .word 4
return_address: .space 4
ra_backup: .space 4
