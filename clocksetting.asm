/*
 * lab12clocksetting.asm
 *
 *  Created: 12/3/2014 6:32:52 PM
 *   Author: Kathryn Blecher
 */ 

 /*
 * labEleven.asm
 *
 *  Created: 11/19/2014 6:06:27 PM
 *   Author: Kathryn Blecher
 */ 

//setup ports
	ldi r20,0b00000011		;1 for output pins, 0 for inputs
	out ddrc,r20			;program pc0 and pc1 as outputs
	ldi r20,0xff			;load 11111111 in r20
	out ddrd,r20			;program all of portd as outputs

//setup portb
	ldi r16,0xfe			;turn pnp transistors off, pullups on
	out portb,r16
	ldi r16,0b11000111		
	out ddrb,r16			;program all of portb as outputs

	.def ticks = r16		;set the register name to ticks
	.def secs = r17			;set the register name to secs
	.def minutes = r18		;set the register name to minutes 
	.def hours = r19		;set the regsiter name to hours
	.def temp = r23			;for LED dimness

	clr ticks				;initialize ticks so that there is nothing in it
	clr secs				;initialize secs for the same reason
	clr minutes				;initialize minutes for the same reason
	clr hours				;initialize hours for the same reason


tick:						;the label for the tick part of the program
	sbis pinc, 7			;checks bit 7 of portc
	rjmp tick				;jumb back if zero
	inc ticks				;increment register 16 
	cpi ticks, 60			;compare ticks to see if it has reached 60
	brne tock				;jump if not 60
	clr ticks				;reset ticks				
	

	inc secs				;increment the seconds count
	sbis pinb,4			;checks to see if increment button is pressed
	rjmp increment			;sets minutes
	sbis pinb,5			;checks to see if increment button is pressed
	rjmp set_hours			;sets hours


	cpi secs, 60			;compare ticks to see if it has reached 60
	brne tock				;jump to tock if seconds is not 60
	clr secs				;clear seconds counter
	
	inc minutes				;increment minutes counter
	cpi minutes, 60         ;checking if minutes is 60
	brne tock				;this will jump to tock
	clr minutes				;resets minutes

	inc hours				;increment the hours counter
	cpi hours, 24			;compare hours to see if it has reached 24
	brne tock				;this will jump to tock if it has not reached
	clr hours				;resets hours 

tock:
	com secs				;invert bits
	out portd, secs			;sends bits to port d
	CBI portb,0				;enable digit zero (turns on lights)
	RCALL delay				;dim brightness
	rcall delay				
	
	ldi temp,0b00111111

	com secs				;invert bits
	out portd, temp			;sends bits to port d
	CBI portb,0				;enable digit zero (turns on lights)
	rcall delay_19k
	SBI portb,0				;disable digit zero


	mov r20, hours			;moving the hours to r20
	RCALL get_segs			;call get_segs
	OUT portd,r21			;Move data from r21 to port d

	CBI portd,6				;clear bit 6 of port d
	CBI portd,7				;clear bit 7 of port d

//digit one
	CBI portb,1				;enable digit 1
	RCALL delay_21k			;delay for 21000 clocks
	SBI portb,1				;disable digit 1
	OUT portd, r22			;move data from r22 to port D

	CBI portd,6				;clear bit 6 of port d
	CBI portd,7				;clear bit 7 of port d


//digit two
	CBI portb,2				;enable digit 2
	RCALL delay_21k			;delay for 21000 clocks
	SBI portb,2				;disable digit 2

//displaying time for the minutes digits 
	mov r20, minutes		;moving minutes to r20
	rcall get_segs			;display minutes
	RCALL delay_21k			;delay for 21000 clocks 
	OUT portd, r21			;move data from r21 to port D

	CBI portd,6				;clear bit 6 of port d
	CBI portd,7				;clear bit 7 of port d

//digit three
	CBI portb,6				;enable digit 3
	RCALL delay_21k			;delay for 21000 clocks
	SBI portb,6				;disable digit 3
	out portd, r22			;move data from r22 to port D

	CBI portd,6				;clear bit 6 of port d
	CBI portd,7				;clear bit 7 of port d

//digit four
	CBI portb,7				;enable digit 4
	RCALL delay_21k			;delay for 21000 clocks 
	SBI portb,7				;disable digit 4 

	sbic pinc,7				;check bit 7 of portc
	rjmp tock				;wait for zero
	rjmp tick				;do this all over again

	
	
	/*get_segs_m takes an argument in scr and returns the most significant LED
  segments (segs_h) and least significant LED segment (segs_l). The scr 
  register is 0 to 59 for minutes 
  
  		writes: clr, tens,zl, zh, segs_l, segs_h	*/
get_segs:
	clr r0				;clear tens register
div_m:
	cpi r20,10			;bigger than ten?		
	brlo done_div_m		;	no, done
	inc r0				;increment tens count
	subi r20,10			;subtract 10 from scr 
	rjmp div_m          ;repeat until r20 is less than 10
done_div_m:
	ldi zh,high(seg_table<<1)	;load z register for indirect read
	ldi zl,low(seg_table<<1)
	add zl,r0			;compute offset into the table
	brcc nc1_m			;carry required?
	inc zh				;carry
nc1_m:
	lpm r21,z			;read segments from data table
	ldi zh,high(seg_table<<1)	;load z register for indirect read
	ldi zl,low(seg_table<<1)
	add zl,r20			;compute offset into the table
	brcc nc2_m			;carry required?
	inc zh				;carry
nc2_m:
	lpm r22,z			;read segments from data table
	ret	                ;return


// delay_21k simply calls delay 21 times
delay_19k:
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	ret


delay_21k:
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	rcall delay
	ret


// delay takes 1003 cycles to execute
delay:               	;the label that identifies the subroutine to the assembler
    clr r31             ;initialize register 31 (this can be any unused register)
delay_loop:             ;a label for the brne instruction
    inc r31             ;increment r31
    cpi r31,0xf9        ;compare r31 to the final value
    brne delay_loop     ;jump to delay_loop if the count has not reached the final value
    ret                 ;return. The microcontroller 'remembers' where it came from


	// initialize LED segment table
seg_table:		
	.dw 0xeb09				;1,0
	.dw 0xc185				;3,2
	.dw	0x5163				;5,4
	.dw 0xcb11				;7,6
	.dw 0x4101				;9,8
	.dw 0x00ff				;on,off

increment:
clr secs					;sets seconds to 0
inc minutes					;adds 1 to minutes
cpi minutes,60				;checks if minutes = 0
brne increment				;jumps back if not 60
clr minutes					;sets minutes to zero if 60
rjmp tock					;jumps to display the time

set_hours:
inc hours    				;adds 1 to hours
cpi hours,24				;checks if hours = 24
brne set_hours				;jumps back if not 24
clr hours					;sets hours to zero if 24
rjmp tock					;jumps to display the time
