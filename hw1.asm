# Homework #1
# name: Kathryn Blecher
# sbuid: 108871623

.data
str: .asciiz "Enter an integer number: "
twos: .asciiz "2's compliment:      "
sign: .asciiz "Sign magnitude:      "
neg: .asciiz "Neg 2's compliment:  "
newline: .asciiz "\n"
tab: .asciiz "   "
none: .asciiz "  does not exist\n"
plus: .asciiz "+1."
minus: .asciiz "-1."
exp: .asciiz " * 2^"
ieee: .asciiz "IEEE-754 single precision: "
little: .asciiz "\nlittle endian : "
big: .asciiz "\nbig endian: "
x: .word 0
y: .word 0

.text
.globl start

start:
li $t0, 0	  # initializes t0, tracks what loop user is in
la $a0, str 	  # put string address in register $a0
li $v0, 4	  # load call to print string
syscall
li $v0, 5	  # load call to accept an int from the user input
syscall			
sw $v0, x	  # store the input in memory
sw $v0, y
la $a0, twos	  # put the string into $a0
li $v0, 4	  # load call to print string
syscall
la $t1, x
loop1:
lw $a0, x 	  # put the value of x into $a0
li $v0, 1	  # load call to print integer
syscall
loop2:
la $a0, tab       # inserts a tab
li $v0, 4	  
syscall
lw $a0, x         # prints hex version
li $v0, 34	  
syscall
la $a0, newline   # returns to a new line
li $v0, 4	  
syscall
lw $a0, x	  # prints out x in binary
li $v0, 35
syscall
la $a0, tab	  # prints out tab
li $v0, 4
syscall
lw $a0, x	  # prints out x normally
li $v0, 1
syscall
la $a0, newline   # returns to a new line
li $v0, 4	  
syscall
lw $t1, x	    # puts x back into $t1
beq $t0, 2, ieee754 # checks if this is final loop
beq $t0, 1, neg_two # checks if this is second time through loop
li $t5, -2147483648 # loads maximum neg value into $t5
beq $t1, $t5, inval # checks to see if x is max neg value, goes to inval if so
la $a0, sign	    # put the string (sign mag) into $a0
li $v0, 4	    # load call to print string
syscall
li $t0, 1
bgt $t1, -1, loop1  # if x is nonnegative, repeat the previous loop (sign mag=2's comp)
lw $a0, x	    # prints out x normally
li $v0, 1
syscall
li $t2, -1	  # loads -1 into $t2
mult $t2, $t1     # multiplies x with -1 to make the negative x be positive
mfhi $t3
mflo $t4
ori $t6, $t4, 10000000000000000000000000000000  #takes the positive value and puts a 1 in the sign bit
la $a0, tab       # inserts a tab
li $v0, 4	  
syscall
move $a0, $t6     # prints hex version
li $v0, 34	  
syscall
la $a0, newline   # returns to a new line
li $v0, 4	  
syscall
move $a0, $t6	  # puts negative x into $a0
li $v0, 35	  # load call to print string
syscall
la $a0, newline   # returns to a new line
li $v0, 4	  
syscall

neg_two:
lw $t1, x
li $t2, -1	  # loads -1 into $t1
mult $t2, $t1     # multiplies x with -1
mfhi $t3
mflo $t4
la $a0, neg	  # put the string (neg 2's comp) into $a0
li $v0, 4	  # load call to print string
syscall
move $a0, $t4	  # puts negative x int $a0
li $v0, 1	  # load call to print string
syscall
li $t0, 2
sw $t4, x
b loop2

ieee754:
li $t0, -2147483648  # loads 1000000000000000000000000000000 into $t0
lw $t1, y	     # loads x into $t1
and $t0, $t0, $t1    # extracts sign bit
srl $t0, $t0, 31     # $t0 = sign
la $a0, ieee	     # put the string (ieee) into $a0
li $v0, 4	     # load call to print string
syscall 
beqz $t0 pos	     # if the number is positive it goes to print +
la $a0, minus	     # else prints the negative sign
li $v0, 4
syscall
b next
pos:
la $a0, plus         # prints out pos sign
li $v0, 4
move $t3, $t1        # puts x in to $t3
syscall
next:
li $t0, 8388607      # loads 00000000011111111111111111111111 into $t0
move $t3, $t1        # puts x in to $t3
and $t0, $t0, $t3    # extracts the fractional part
sll $t0, $t0, 9
move $a0, $t0	     # put the fractional string in $a0
li $v0, 35
syscall
li $t0, 2139095040   # loads 01111111100000000000000000000000
and $t0, $t0, $t1    # extracts exponent into $t0
srl $t0, $t0, 23     # shifts it over
la $a0, exp
li $v0, 4
syscall
addi $t0, $t0, -127
move $a0, $t0
li $v0, 1
syscall

endian:
la $a0, little		# loads out string (lit end)
li $v0, 4
syscall
lw $t0, y		# sets $t0 to x
li $t1, 255	        # sets $t1 to 00000000000000000000000011111111
and $t1, $t1, $t0       # extracts bits 7-0
move $a0, $t1
li $v0, 36              # prints last 8 bits
syscall 
li $v0, 11              # prints last 8 bits in ascii
syscall 
li $t2, 65280           # sets $t2 to 00000000000000001111111100000000
and $t2, $t2, $t0       # extracts bits 15-8
srl $t2, $t2, 8         # shifts them over 8
la $a0, tab
li $v0, 4
syscall
move $a0, $t2
li $v0, 36		# prints bits 15-8
syscall 
li $v0, 11		# prints bits 15-8 in ascii
syscall 
li $t3, 16711680	# sets $t3 to 00000000111111110000000000000000
and $t3, $t3, $t0       # extracts bits 23-16
srl $t3, $t3, 16        # shifts them over 16
la $a0, tab
li $v0, 4
syscall
move $a0, $t3
li $v0, 36		# prints bits 23-16
syscall 
li $v0, 11		# prints bits 23-16 in ascii
syscall 
li $t4, 4278190080	# sets $t3 to 11111111000000000000000000000000
and $t4, $t4, $t0       # extracts bits 31-24
srl $t4, $t4, 24        # shifts them over 24
la $a0, tab
li $v0, 4
syscall
move $a0, $t4
li $v0, 36		# prints bits 31-24
syscall 
li $v0, 11		# prints bits 31-24 in ascii
syscall 
la $a0, big	        # loads out string (big end)
li $v0, 4
syscall
move $a0, $t4
li $v0, 36		# prints bits 31-24
syscall 
li $v0, 11		# prints bits 31-24 in ascii
syscall 
la $a0, tab
li $v0, 4
syscall
move $a0, $t3
li $v0, 36		# prints bits 23-16
syscall 
li $v0, 11		# prints bits 23-16 in ascii
syscall 
la $a0, tab
li $v0, 4
syscall
move $a0, $t2
li $v0, 36		# prints bits 15-8
syscall 
li $v0, 11		# prints bits 15-8 in ascii
syscall 
la $a0, tab
li $v0, 4
syscall
move $a0, $t1
li $v0, 36              # prints last 8 bits
syscall 
li $v0, 11              # prints last 8 bits in ascii
syscall 
b exit

inval:		  # the print output call for the max neg int
la $a0, sign	  # put the string (sign mag) into $a0
li $v0, 4	  # load call to print string
syscall
la $a0, none	  # put the string (not exist) into $a0
li $v0, 4	  # load call to print string
syscall
la $a0, neg	  # put the string (neg 2's comp) into $a0
li $v0, 4	  # load call to print string
syscall
la $a0, none	  # put the string (not exist) into $a0
li $v0, 4	  # load call to print string
syscall
b ieee754

exit:
li $v0, 10
syscall
