##############################################################
# Homework #3				 #
# name: Kathryn Blecher				 #
# sbuid: 108871623				 #
#          @@@@@@@@ I DID EXTRA CREDIT @@@@@@@@		 #
##############################################################

parse_escape_code:
	addi $sp, $sp -4
	sw $ra, ($sp)	  # store return address on stack
	move $t0, $a0	  # store a0 address in t0
	li $v1, 15		  # set up byte space
pec_loop:
	li $t2, 1		  # set up flag for foreground boldness
	lb $t1, ($a0)	  # load byte
	addi $a0, $a0, 1
	beq $t1, ';', pec_loop	  # keep lookin!
	beq $t1, 'm', pec_exit	  # leaves when m is found
	beqz $t1, pec_false
	beq $t1, '4', background  # set up background byte
	beq $t1, '3', foreground  # set up foreground byte
	beq $t1, '0', not_bold
	beq $t1, '1', bold
	b pec_false
background: 
	jal atoi		   # get number
	beq $v0, 9, pec_false	   # if 9, default, go to end	
	sll $v0, $v0, 4	   # shift left 4
	or $v1, $v1, $v0	   # or with char
	addi $a0, $a0, 1	   # move address forward
	j pec_loop
foreground: 
	jal atoi		   # get number
	beq $v0, 9, pec_false	   # if 9, default, exit
	or $v1, $v1, $v0	   # or with char
	beqz $t2, fg_end	   # if fg is bold, do nothing	
	andi $v1, $v1, -9	   # if not, get rid of the 1 at bit 3
fg_end:
	addi $a0, $a0, 1	   # move up one so next call doesn't take num
	j pec_loop
bold: 
	addi $a0, $a0, 1
	lb $t1, ($a0)	  # load up byte
	addi $a0, $a0, 1	  # moving forward
	beq $t1, '4', bg_true
	andi $v1, $v1, -8
	li $t2, 0		  # flag showing fg is bold
	beq $t1, '3', foreground  # go to foreground
	j pec_false		  # exit loop if no 3 or 4 found after bold 
bg_true:
	ori $v1, $v1, 128
	j background
not_bold:	
	addi $a0, $a0, 1
	lb $t1, ($a0)	  # load up byte
	addi $a0, $a0, 1	  # moving forward
	beq $t1, '4', background  # do nothing
	andi $v1, $v1, -16
	beq $t1, '3', foreground  # jump to foreground
	j pec_false		  # if not a 3 or 4, leave to exit with defaults
pec_exit:
	move $v0, $v1	  # load 8 bit to be returned in v0
	j pec_final
pec_false:
	li $v0, 15		  # if ansi escape is missing an 'm', reset to default
pec_final:
	move $a0, $t0	  # put a0 back
	lw $ra, ($sp)	  # load up ra
	addi $sp, $sp, 4	  # position sp back
	jr $ra
	
printk:
	li $a3, 15		  # default for settings
	li $a1, 0	              # defaul start x coordinate
	li $a2, 0	   	  # default y coordinate
	li $t1, 1		  # loop flag for printk_1
printk_start:	
	addi $sp, $sp, -4
	sw $ra, ($sp)	  # store return address on stack
printk_loop:
	lb $t0, ($a0)	  # load first char into t0
	beqz $t0, exit_print	  # if the char is \, check for ANSI escape
	beq $t0, '\\', printk_check
	jal putck		  # if not \, print to screen
	addi $a0, $a0, 1	  # move byte address forward
	b printk_loop	  # loop 
printk_check:
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 1		  # loop flag for printk_1
	beq $t0, 'n', printk_nl	  # print a new line
	beq $t0, 't', printk_t1   # print a new tab
	beq $t0, 'b', printk_b1	  # print a backspace
	bne $t0, '0', printk_1    # if the byte isnt '0', exit
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 2
	bne $t0, '3', printk_1    # if the byte isnt '3', exit
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 3		  
	bne $t0, '3', printk_1    # if the byte isnt '3', exit
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 4		  
	bne $t0, '[', printk_1    # if the byte isnt '[', exit
	addi $a0, $a0, 1
	jal parse_escape_code     # decode
	move $a3, $v0	  # put output info into a1
pass_m1:
	lb $t0, ($a0)	  # loop until we pass m so we can continue printing
	addi $a0, $a0, 1
	beq $t0, 'm' printk_loop
	b pass_m1
printk_1:
	sub $a0, $a0, $t1	  # move address back to '\'
	lb $t0, ($a0)	  # load up '\'
	jal putck		  # print '\'
	addi $a0, $a0, 1          # move address forward
	j printk_loop
printk_nl:
	addi $a0, $a0, 1	  # nmove address forward
	li $a1, 0		  # set coords back to 0
	li $a2, 0
	li $t4,4294901760	  # load start MMIO memory address
nl_loop:
	lw $t5, ($t4)	  # load up start char
	beqz $t5, nl_done	  # if NULL, end
	addi $t4, $t4, 160	  # else, add 160 (move from 0,0 to 0,1... 0,2...)
	addi $a2, $a2, 1	  # move y
	j nl_loop		  
nl_done:
	jal putck_xy	  # print n go
	addi $a0, $a0, 1	  # move address up
	j printk_loop
printk_t1:
	move $t3, $a0	  # save a0 in t3
	la $a0, tabx	  # load tab str address
	jal printk_start	  # call print on tab str
	move $a0, $t3	 # put a0 back
	addi $a0, $a0, 1	 # move forward 1
	j printk_loop	
printk_b1:
	li $a1, 0		 # sets default coordinates to 0,0
	li $a2, 0
	li $t4,4294901758	 # load mem address 2 before start of MMIO
bl_loop:
	addi $t4, $t4, 2	 # add 2 to MMIO address
	lh $t5, ($t4)	 # load half word
	beqz $t5, b1_done	 # if t5 is NULL we're done
	addi $a1, $a1, 1	 # else, move mem address forward
	beq $a1, 80, b1_nl	 # if a1=80, reset to 0
	j bl_loop
b1_nl:
	li $a1, 0
	addi $a2, $a2, 1
	j bl_loop
b1_done:
	addi $a0, $a0, 1
	addi $a1, $a1, -1	   # move x back 1
	move $t6, $a0	   # save a0 in t6
	la $a0, tabx	   # load tab str address
	jal putck_xy
	move $a0, $t6	   # put a0 back
	jal putck_xy
	addi $a0, $a0, 1	   # move forward 1
	j printk_loop
exit_print:
	lw $ra, ($sp)	  # load ra back
	addi $sp, $sp, 4
	jr $ra
	
putk:
	addi $sp, $sp, -4
	sw $ra, ($sp)	  # store ra on stack
	li $a3, 15 		  # load 00001111
	jal putck		  # go to putck
	lw $ra, ($sp)	  # restore ra
	addi $sp,$sp, 4
	jr $ra
	
putck:
	li $t0, 4294901758	  # load fffefffe into t0
	li $t2, 4294905760
putck_loop:
	addi $t0, $t0, 2	  # increases from here
	beq $t0, $t2, putck_end   # if reach end of MMIO, leave
	lh $t1, ($t0)	  # load the halfword at this address
	bnez $t1, putck_loop	  # if = 0, found cursor
putck_set:
	move $t1, $a3
	sll $a3, $a3, 8	  # shift a0
	lb $t8, ($a0)
	or $a3, $a3, $t8	  # combine a0 & a1
	sh $a3, 0($t0)	  # load into MMIO
	move $a3, $t1
putck_end:
	jr $ra

putck_xy:
	addi $sp, $sp, -4
	sw $ra, ($sp)	    # load ra on stack
	li $t0, 2		    # load 2 into t0
	mult $a1, $t0	    # multiply x by 2
	mflo $t1		    # put x position in t1
	li $t0, 160		    # put 160 in t0
	mult $a2, $t0	    # multiply y by 160
	mflo $t2
	add $t3, $t2, $t1	    # add x & y
	li $t0, 4294901760	    # load fffefffe into t0
	add $t3, $t3, $t0	    # add from starting address in MMIO
putck_xy_loop:
	beq $t0, $t3, putck_xy_exit # if they are equal, we are where we want to be
	lh $t4, ($t0)	    # load whats at mem address
	beqz $t4, insert_space
	addi $t0, $t0, 2	    # move forward 2
	b putck_xy_loop
insert_space: 
	li $t5, 32	    	    # inserts a blank space if spot is NULL
	sll $v0, $a3, 8
	or $t5, $t5, $v0
	sh $t5, ($t0)
	b putck_xy_loop	    # returns back to loop
putck_xy_exit:
	jal putck_set
	lw $ra, ($sp)	    # put ra and sp back
	addi $sp, $sp, 4
	jr $ra
	
putk_xy:
	addi $sp, $sp, -4
	sw $ra, ($sp)	# set up ra and sp
	addi $sp, $sp, -4
	sw $a0, 4($sp)
	li $a1, 15
	jal putck_xy	# call putck_xy with default settings
	lw $a0, ($sp)
	lw $ra, 4($sp)
	addi $sp, $sp, 4
	jr $ra
	
printk_xy:
	li $a3, 15	               # put default ANSI in a1
	addi $sp, $sp, -4
	sw $ra, ($sp)	   # store return address on stack
printk_xy_loop:
	beq $a1, 80, nextline
	lb $t0, ($a0)	   # load first char into t0
	beqz $t0, exit_printk_xy   # if the char is NULL, exit loop
	beq $t0, '\\', printk_xy_check  # go check for escapes
	jal putck_xy     	   # if not NULL or \, print to screen
	addi $a0, $a0, 1	   # move byte address forward
	addi $a1, $a1, 1	   # move x over
	b printk_xy_loop	   # loop 
printk_xy_check:
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 1		  # loop flag for printk_1
	beq $t0, 'n', nextline    # print a new line
	beq $t0, 't', tab         # print a new tab
	beq $t0, 'b', backspace   # print a backspace
	bne $t0, '0', printk_xy1  # if the byte isnt '0', exit
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 2
	bne $t0, '3', printk_xy1  # if the byte isnt '3', exit
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 3		  
	bne $t0, '3', printk_xy1  # if the byte isnt '3', exit
	addi $a0, $a0, 1	  # move address forward 1
	lb $t0, ($a0)	  # load byte into t1
	li $t1, 4		  
	bne $t0, '[', printk_xy1  # if the byte isnt '[', exit
	addi $a0, $a0, 1
	jal parse_escape_code     # decode
	move $a3, $v0	  # put output info into a3
pass_m:
	lb $t0, ($a0)	   # move past m to start printing again
	addi $a0, $a0,1
	beq $t0, 'm', printk_xy_loop
	b pass_m
printk_xy1:
	sub $a0, $a0, $t1	  # move address back to '\'
	lb $t0, ($a0)	  # load up '\'
	jal putck_set	  # print '\'
	addi $a0, $a0, 1          # move address forward
	j printk_xy_loop
nextline:
	li $a1, 0		  # set x back to 0
	addi $a2, $a2, 1	  # y++
	beq $a2, 25, reset_printk # check to see if y=25
	addi $a0, $a0, 1
	b printk_xy_loop	  # go back
tab:
	addi $a0, $a0, 1
	addi $a1, $a1, 5	# puts 5 spaces
	li $t9, 80		# load up 80
	sge $t8, $a1, $t9	# check to see if tab went over 79
	mult $t9, $t8	# multiply 80 by either 1 or 0
	mflo $t9
	sub $a1, $a1, $t9	# if below 80, minus 0, >=80, subtract 80
	add $a2, $a2, $t8
	j printk_xy_loop	
backspace:
	beqz $a1, if_start	# sets x to 80 if x=0
	addi $a1, $a1, -1	# move x back
	move $t6, $a0	# save a0 in a temp
	la $a0, tabx	# load up tab address
	jal putck_xy	# insert a space where you want char gone
	move $a0, $t6	# put a0 address back
	addi $a0, $a0, 1	# move string add forward
	jal putck_xy	# load up next char if available
	addi $a0, $a0, 1	# move address forward
	addi $a1, $a1, 1	# move x forward
	j printk_xy_loop
if_start:
	li $a1, 80		# go to end of x
	beqz $a2, if_start2	# jump to go to end of y
	addi $a2, $a2, -1	# move back
	j backspace
if_start2:  
	li $a2, 24		# put y as last row
	j backspace
reset_printk:
	li $a2, 0		# put y as first row
	b printk_xy_loop
exit_printk_xy:
	lw $ra, ($sp)	  # load ra back
	addi $sp, $sp, 4
	jr $ra
		
clear:
	li $t0, 4294901760	  # load fffefffe into t0
	li $t1, 4294905760 
clear_loop:
	sh $0, ($t0)	  # load the halfword at this address
	addi $t0, $t0, 2	  # initial add goes to ffff0000, and increases from here
	bne $t0, $t1, clear_loop	
	jr $ra
	
ec:	
	addi $sp, $sp, -4	# set up return address and sp
	sw $ra, ($sp)
	jal clear		# clear screen
	la $a0, ec1		
	li $a1, 0
	li $a2, 0
	jal printk_xy	# print out all ec strings
	la $a0, ec2
	jal printk_xy
	la $a0, ec3
	jal printk_xy
	la $a0, ec4
	jal printk_xy
	la $a0, ec5
	jal printk_xy	
	la $a0, ec6
	jal printk_xy	
	la $a0, ec7
	jal printk_xy
	la $a0, ec8
	jal printk_xy
	la $a0, ec9
	jal printk_xy
	la $a0, ec10
	jal printk_xy
	la $a0, ec11
	jal printk_xy
	la $a0, ec12
	jal printk_xy
	la $a0, ec13
	jal printk_xy
	la $a0, ec14
	jal printk_xy
	la $a0, ec15
	jal printk_xy
	la $a0, ec16
	jal printk_xy
	lw $ra, ($sp)
	addi $sp, $sp, 4
	jr $ra
	
# Converts a string to an integer.
atoi:
	beqz $a0, atoi_done		# Check for NULL (0x0)
	move $t7, $a0
	li $v0, 0
	li $t9, 10
	li $t8, 0				# If this value is 1 then number is negative, else positive
	# Check for negative sign
	lb $t6, 0($t7)
	bne $t6, '-', atoi_loop
	# There was a negative sign so add 1 to pointer and mark this was negative
	addi $t7, $t7, 1
	li $t8, 1
atoi_loop:
	lb $t6, 0($t7)
	blt $t6, '0', atoi_sign				# Check to see if we have a valid character >= '0'
	bgt $t6, '9', atoi_sign				# Check to see if we have a valid characyer <= '9'
	# Subtract character zero from ASCII character
	addi $t6, $t6, -48
	# multiple sum * 10
	mult $v0, $t9
	mflo $v0
	# Add c - '0' to the sum
	add $v0, $v0, $t6
	# increment the address by 1
	addi $t7, $t7, 1
	j atoi_loop
atoi_sign:
	# Check to see if we need to negate the number
	bne $t8, 1, atoi_done
	# else we need to negate the final value
	not $v0, $v0			# Flip all the bits
	addi $v0, $v0, 1		# Add 1
atoi_done:
	jr $ra
	
.data
tabx: .asciiz "     "
ec1: .asciiz "\\033[44m     \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t \\033[1;41m\\t\\t  \\033[44m     \\033[1;41m  \\t  \\033[44m \\033[1;41m     \\t \\033[0;44m   \\t\\t\\t\\t  \\t\\t\\t   \\033[1;41m \\033[1;43m     \\t \\033[44m     \\033[1;41m \\033[1;43m  \\t \\033[0;44m \\033[1;41m \\033[1;43m  \\t  \\033[1;41m \\033[0;44m     \\t\\t\\t\\t\\t\\t\\t   \\033[1;41m \\033[1;43m \\033[1;42m   \\t  \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\t \\033[44m \\033[1;41m \\033[1;43m \\033[1;42m \\t \\033[1;43m \\033[1;41m \\033[0;44m \\t\\t\\t\\t\\t\\t\\t\\t"  
ec2: .asciiz "\\033[44m \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m  \\t  \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m      \\033[44m \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m     \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m\\t\\t     \\033[40m\\t\\t\\t  \\033[0;44m  \\t    \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m\\t   \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m     \\033[44m \\033[1;41m "
ec3: .asciiz "\\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m \\033[40m \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m \\t\\t   \\033[40m \\033[43m \\t\\t\\t \\033[40m \\033[44m \\t    \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m \\033[44m \\t      \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m   \\033[1;46m \\033[1;42m \\033[1;43m "
ec4: .asciiz "\\033[1;41m \\033[0;44m  \\t   \\033[1;41m   \\033[40m \\033[43m   \\033[1;45m  \\t\\t \\033[43m   \\033[40m \\033[44m  \\t \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m \\t  \\033[44m \\033[1;41m     \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m \\033[44m\\t     \\033[1;41m\\t    \\033[40m \\033[43m  \\033[1;45m\\t \\033[45m \\033[1;45m  \\033[45m \\033[1;45m     \\033[43m  \\033[40m \\033[44m \\t   " 
ec5: .asciiz "\\033[1;41m \\033[1;43m \\033[1;42m  \\t   \\033[44m \\033[1;43m      \\033[1;42m \\033[1;46m \\033[1;45m \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\t  \\033[44m    \\033[1;41m\\t    \\033[40m \\033[43m \\033[1;45m  \\033[45m \\033[1;45m\\t\\t    \\033[43m \\033[40m \\033[44m\\t    \\033[1;41m \\033[1;43m\\t\\t \\033[44m \\033[1;42m\\t  \\033[1;46m \\033[1;45m \\033[44m     \\033[1;41m \\033[1;43m\\t     \\033[44m   \\033[1;41m    \\033[1;43m\\t "
ec6: .asciiz "\\033[40m \\033[43m \\033[1;45m     \\t \\033[40m  \\033[1;45m \\033[45m \\033[1;45m  \\033[43m \\033[40m \\033[44m  \\033[40m  \\033[44m     \\033[1;41m\\t\\t  \\033[44m \\033[1;45m \\t   \\033[44m     \\033[1;41m\\t\\t \\033[44m   \\033[1;41m  \\033[1;43m    \\033[1;42m    \\033[40m \\033[43m \\033[1;45m\\t     \\033[40m \\033[1;40m  \\033[40m \\033[1;45m   \\033[43m \\033[40m \\033[44m \\033[40m \\033[1;40m  \\033[40m \\033[44m \\t\\t\\t\\t\\t\\t\\t\\t   "
ec7: .asciiz "\\033[1;41m  \\033[1;43m    \\033[1;42m    \\033[40m \\033[0;43m \\033[1;45m\\t \\033[45m \\033[1;45m   \\033[40m \\033[1;40m   \\033[40m \\033[1;45m  \\033[43m \\033[40m  \\033[1;40m   \\033[40m \\033[44m    \\033[1;41m\\t    \\033[44m    \\033[1;41m\\t    \\033[44m     \\033[1;41m\\t\\t \\033[44m  \\033[1;41m   \\033[1;43m  \\033[1;42m\\t \\033[40m \\033[43m \\033[1;45m   \\033[45m \\033[1;45m\\t \\033[40m \\033[1;40m    \\033[40m    \\033[1;40m    \\033[40m \\033[44m    "
ec8: .asciiz "\\033[1;43m  \\t \\033[1;41m \\033[44m   \\033[44m \\033[1;43m\\t   \\033[1;41m \\033[44m     \\033[1;41m \\033[1;43m\\t    \\033[1;41m \\033[44m  \\033[1;41m \\033[1;43m    \\033[1;42m     \\033[40m  \\033[43m \\033[1;45m\\t  \\033[45m \\033[1;45m \\033[40m \\033[1;40m\\t\\t    \\033[40m \\033[44m   \\033[1;42m\\t  \\033[1;43m \\033[1;41m \\033[44m    \\033[1;42m\\t  \\033[1;43m \\033[1;41m \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m\\t  "
ec9: .asciiz "\\033[1;43m \\033[1;41m \\033[44m  \\033[1;41m \\033[1;43m   \\033[1;42m   \\033[40m     \\033[43m \\033[1;45m \\033[45m \\033[1;45m\\t  \\033[40m \\033[1;40m \\t\\t   \\033[40m \\033[44m   \\033[1;46m\\t \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m    \\033[1;46m\\t \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m     \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m \\033[1;41m  \\033[1;43m  \\033[1;42m  "
ec10: .asciiz "\\033[40m  \\033[1;40m    \\033[40m \\033[43m \\033[1;45m\\t    \\033[40m \\033[1;40m   \\033[1;47m \\033[40m \\033[1;40m     \\033[1;47m \\033[40m \\033[1;40m  \\033[40m \\033[44m   \\033[1;45m     \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m    \\033[1;45m     \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m  \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m "
ec11: .asciiz "\\033[44m \\033[1;41m \\033[1;43m   \\033[1;42m  \\033[0;40m \\033[1;40m   \\033[40m   \\033[43m \\033[1;45m     \\033[45m \\033[1;45m   \\033[40m \\033[1;40m   \\033[40m  \\033[1;40m   \\033[40m \\033[1;40m \\033[40m  \\033[1;40m  \\033[40m \\033[44m\\t \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m\\t   \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m     \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m \\033[1;45m \\033[44m "
ec12: .asciiz "\\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m \\033[44m \\033[1;41m \\033[1;43m  \\033[1;42m   \\033[1;46m \\033[40m    \\033[1;45m \\033[40m \\033[43m \\033[1;45m \\t   \\033[40m \\033[1;40m \\033[1;41m  \\033[1;40m\\t    \\033[1;41m  \\033[40m \\033[44m\\t \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m    \\033[44m     \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m     \\033[44m \\033[1;41m \\033[1;43m \\033[1;42m \\033[1;46m "
ec13: .asciiz "\\033[1;45m   \\033[1;46m \\033[1;42m \\033[1;43m \\033[1;41m   \\033[1;43m  \\033[1;42m  \\033[1;46m    \\033[1;45m   \\033[40m \\033[43m  \\033[1;45m \\033[45m \\033[1;45m      \\033[40m \\033[1;40m \\033[1;41m  \\033[1;40m \\033[40m \\033[1;40m  \\033[40m \\033[1;40m  \\033[40m \\033[1;40m \\033[1;41m  \\033[40m \\033[44m\\t \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m     \\033[44m     \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m\\t \\033[44m \\033[1;41m \\033[1;43m "
ec14: .asciiz "\\033[1;42m \\033[1;46m   \\033[1;45m \\033[1;46m \\033[1;42m \\033[1;43m     \\033[1;42m  \\033[1;46m    \\033[1;45m    \\033[40m \\033[43m   \\033[1;45m\\t   \\033[40m \\033[1;40m   \\033[40m\\t  \\033[1;40m  \\033[40m \\033[0;44m\\t  \\033[1;45m \\033[1;46m \\033[1;42m\\t \\033[44m     \\033[1;45m \\033[1;46m \\033[1;42m\\t  \\033[44m \\033[1;41m \\033[1;43m \\033[1;42m    \\033[1;45m \\033[1;46m \\033[1;42m\\t \\033[1;46m    \\033[1;45m\\t \\033[40m  \\033[43m\\t\\t "
ec15: .asciiz "\\033[40m \\033[1;40m\\t     \\033[40m \\033[44m\\t   \\033[1;45m \\033[1;46m\\t  \\033[44m     \\033[1;45m \\033[1;46m\\t   \\033[44m \\033[1;41m \\033[1;43m     \\033[1;45m \\033[1;46m\\t   \\033[1;45m\\t \\033[44m   \\033[40m \\033[1;40m \\033[40m\\t\\t\\t\\t  \\033[44m\\t    \\033[1;45m\\t   \\033[44m     \\033[1;45m\\t    \\033[44m \\033[1;41m\\t \\033[1;45m\\t\\t  \\033[44m\\t \\033[40m \\033[1;40m  \\033[40m \\033[44m \\033[40m \\033[1;40m  \\033[40m \\033[44m\\t \\033[40m "
ec16: .asciiz "\\033[1;40m  \\033[0;40m \\033[44m \\033[40m \\033[1;40m  \\033[40m \\033[44m \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t     \\033[40m   \\033[44m   \\033[40m   \\033[44m       \\033[40m   \\033[44m  \\033[40m   \\033[44m \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t   "
